#include <SFML/Graphics.hpp>

int main() {
    sf::RenderWindow window(sf::VideoMode(800, 600), "SFML Rectangle with Circles");

    // Rectangle
    sf::RectangleShape rectangle(sf::Vector2f(400, 200));
    rectangle.setPosition(200, 200);
    rectangle.setFillColor(sf::Color::Blue);

    // Circles
    sf::CircleShape circle1(50); // Circle 1
    circle1.setFillColor(sf::Color::Red);
    circle1.setPosition(220, 220);

    sf::CircleShape circle2(50); // Circle 2
    circle2.setFillColor(sf::Color::Green);
    circle2.setPosition(350, 220);

    sf::CircleShape circle3(50); // Circle 3
    circle3.setFillColor(sf::Color::Yellow);
    circle3.setPosition(480, 220);

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear(sf::Color::White);
        window.draw(rectangle);
        window.draw(circle1);
        window.draw(circle2);
        window.draw(circle3);
        window.display();
    }

    return 0;
}

