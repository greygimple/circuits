// src/not_gate.cc
#include "../include/not_gate.h"
#include <tuple>

NotGate::NotGate(int inCount, int outCount) : Gate(inCount, outCount) {
	// Nothing
}

bool NotGate::eval(void) {
	bool result = !(this->inputs.getValue(0));
	
	// Check for any true values in the inputs
	bool success = this->outputs.setValue(result);

	return success;
}

void NotGate::print(int index) {
	std::cout << "Gate List Index : " << index << std::endl;
	std::cout << "Not Gate" << std::endl;
	std::cout << "Input Values" << std::endl;

	for (int i = 0; i < this->inputs.ioCount - 1; ++i) {
		std::cout << this->inputs.getValue(i) << ", ";
	}

	std::cout << this->inputs.getValue(this->inputs.ioCount) << std::endl;

	std::cout << "Output Values" << std::endl;

	std::cout << this->inputs.getValue() << std::endl << std::endl;
}
