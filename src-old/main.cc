// src/main.cc
#include <iostream>
#include "../include/board.h"
#include "../include/gate.h"
#include "../include/gate_io.h"
#include "../include/input_gate.h"
#include "../include/and_gate.h"
#include "../include/or_gate.h"
#include "../include/not_gate.h"

using namespace std;

int main() {
	// Create Board & set all inputs to 1
	Board board(2, 1, 1);
	board.setInput(0, 0);
	board.setInput(1, 0);

	// Add some gates
	board.addGate<NotGate>(1, 1); // 2
	board.addGate<OrGate>(2, 1);  // 3
	
	//// Add wires to connect board together
	// Board input to not gate
	board.addWire(0, 0, 2, 0, 0);
	
	// Board input to or gate
	board.addWire(1, 0, 3, 1, 1);

	// Not gate to or gate
	board.addWire(2, 2, 3, 0, 0);

	// Or gate to outputs
	board.addWire(3, 3, 1, 0, 0);
	
	// Run Board
	board.runBoard(0);
	return 0;
}

