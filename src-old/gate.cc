// src/gate.cc
#include <iostream>
#include "../include/gate.h"
#include "../include/gate_io.h"

Gate::Gate(int inCount, int outCount) : inputs(inCount), outputs(outCount) {
	this->inputs.ioCount = inCount;
	if (outCount != 0) {
		this->outputs.ioCount = outCount;
	}
}

