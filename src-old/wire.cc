// src/wire.cc
#include "../include/wire.h"
#include "../include/gate.h"

Wire::Wire() {
	g1 = nullptr;
	g2 = nullptr;
	i1 = 0;
	i2 = 0;
}
