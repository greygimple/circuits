// src/board.cc
#include <chrono>
#include <thread>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include "../include/board.h"
#include "../include/gate.h"
#include "../include/gate_io.h"
#include "../include/wire.h"

// Object Init
Board::Board(int inCount, int outCount, int timestep) {
	gateLen = 8;
	gateCount = 0;
	gates = new Gate*[gateLen];

	wireLen = 16;
	wireCount = 0;
	wires = new Wire*[wireLen];

	addGate<InputGate>(inCount);
	addGate<InputGate>(outCount);
}

Board::~Board() {
	// Clean-up heap array of gates
	for (int i = 0; i < gateLen; ++i) {
		delete gates[i];
	}

	delete[] gates;

	for (int i = 0; i < wireLen; ++i) {
		delete wires[i];
	}

	delete[] wires;
}

// Private
// Sort list of wires by their order
void Board::sortWires(void) {
	std::sort(wires, wires + wireCount, [](Wire* a, Wire* b) {
		return a->order < b->order;
	});
}

// Public
// Check if a given index is within the valid range
bool Board::validIndex(int index) const {
	if (0 <= index && index <= gateCount) {
		return 1;
	} else {
		return 0;
	}
}

// Set all input values
void Board::setInput(bool value) {
	gates[0]->inputs.setValue(value);
	gates[0]->outputs.setValue(value);
}

// Set given input value
void Board::setInput(int index, bool value) {
	if (validIndex(index)) {
		gates[0]->inputs.setValue(index, value);
		gates[0]->outputs.setValue(index, value);
	}
}

// Delete gate from the list of gates
bool Board::deleteGate(int index) {
	if (validIndex(index) && index != 0 && index != 1) {
		delete gates[index];
		--gateCount;
		return 1;
	} else {
		return 0;
	}
}

// Set wire connection between two given gates
bool Board::addWire(int order, int g1, int g2, int oSlot, int iSlot) {
	if (order < 0) return 0;
	// std::cout << "order" << std::endl;
	if (!validIndex(g1))  return 0;
	// std::cout << "valid g1" << std::endl;
	if (!validIndex(g2)) return 0;
	// std::cout << "valid g2" << std::endl;
	if (!(gates[g1]->outputs.validIndex(oSlot))) return 0;
	// std::cout << "valid oSlot" << std::endl;
	if (!(gates[g2]->inputs.validIndex(iSlot))) return 0;
	// std::cout << "valid iSlot" << std::endl;

	wires[wireCount] = new Wire();
	
	wires[wireCount]->order = order;
	wires[wireCount]->g1 = gates[g1];
	wires[wireCount]->g2 = gates[g2];
	wires[wireCount]->i1 = oSlot;
	wires[wireCount]->i2 = iSlot;
	++wireCount;
	
	sortWires();

	return 1;
}

bool Board::deleteWire(int index) {
	if (validIndex(index)) {
		delete wires[index];
		--wireCount;
		return 1;
	} else {
		return 0;
	}
}

// Update the entire board by one timestep
bool Board::update(int index, int len) {
	bool success = true;
	
	for(int i = index; i < index + len; ++i) {
		bool s = wires[i]->g2->inputs.setValue(
			wires[i]->i2,
			wires[i]->g1->outputs.getValue(wires[i]->i1)
		);

		wires[i]->g2->eval();
	}

	return success;
}

bool Board::runBoard(int timestep) {
	bool success = true;
	int len = gates[0]->inputs.ioCount;
	
	std::cout << std::endl << "Beginning Board" << std::endl << std::endl;

	for (int i = 0; i < wireCount; ++i) {
		// Display Board each cycle
		std::cout << "Cycle: " << i << std::endl;
		display();
		std::cout << "=====================================================" << std::endl;

		std::this_thread::sleep_for(std::chrono::milliseconds(timestep));
		success &= update(i, len);
		len = 1;
	}

	std::cout << "Final Values:" << std::endl;
	display();
	
	std::cout << "Board Complete" << std::endl << std::endl;

	return success;
}

void Board::display(void) {
	gates[0]->print(0);

	for (int i = 2; i < gateCount; ++i) {
		gates[i]->print(i);
	}
	
	gates[1]->print(1);

	std::cout << std::endl;
}
