// src/gate_io.cc
#include "../include/gate_io.h"
#include "../include/gate.h"
#include <iostream>

// Object Init
GateIO::GateIO(int ioCount) {
	// Clear Array
	for (int i = 0; i < MAX_IO; ++i) {
		this->gateIO[i] = false;
	}

	this->ioCount = ioCount - 1;
}

// Public
// Checks if a given index is in the valid range
bool GateIO::validIndex(int index) const {
	 if (0 <= index && index <= ioCount) {
		return 1;
	 } else {
		return 0;
	 }
}

// Set truth valuation for specific IO slot
bool GateIO::setValue(int index, bool value) {
	if (validIndex(index)) {
		gateIO[index] = value;

		return 1;
	} else {
		return 0;
	}
}

// Set truth valuation for all IO slots
bool GateIO::setValue(bool value) {
	for (int i = 0; i < ioCount; ++i) {
		gateIO[i] = value;
	}

	return 1;
}


// Get IO Value at first location
bool GateIO::getValue(void) {
	return gateIO[0];
}

// Get IO Value at given location
bool GateIO::getValue(int index) {
	return gateIO[index];
}

