// src/and_gate.cc
#include "../include/and_gate.h"
#include <tuple>
#include <iostream>

AndGate::AndGate(int inCount, int outCount) : Gate(inCount, outCount) {
	// Nothing
}

bool AndGate::eval(void) {
	bool result = true;
	
	// Check for any false values in the inputs
	for (int i = 0; i < this->inputs.ioCount; ++i) {
		if (this->inputs.getValue(i) == false) {
			result = false;
			break;
		}
	}

	bool success = this->outputs.setValue(result);
	
	return success;
}

void AndGate::print(int index) {
	std::cout << "Gate List Index : " << index << std::endl;
	std::cout << "And Gate" << std::endl;
	std::cout << "Input Values" << std::endl;

	for (int i = 0; i < this->inputs.ioCount - 1; ++i) {
		std::cout << this->inputs.getValue(i) << ", ";
	}

	std::cout << this->inputs.getValue(this->inputs.ioCount) << std::endl;

	std::cout << "Output Values" << std::endl;

	std::cout << this->inputs.getValue() << std::endl << std::endl;
}
