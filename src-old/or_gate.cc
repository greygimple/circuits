// src/or_gate.cc
#include "../include/or_gate.h"
#include <tuple>

OrGate::OrGate(int inCount, int outCount) : Gate(inCount, outCount) {
	// Nothing
}

bool OrGate::eval(void) {
	bool result = false;
	
	// Check for any true values in the inputs
	for(int i = 0; i < this->inputs.ioCount; ++i) {
		if (this->inputs.getValue(i) == true) {
			result = true;
			break;
		}
	}

	bool success = this->outputs.setValue(result);

	return success;
}

void OrGate::print(int index) {
	std::cout << "Gate List Index : " << index << std::endl;
	std::cout << "Or Gate" << std::endl;
	std::cout << "Input Values" << std::endl;

	for (int i = 0; i < this->inputs.ioCount - 1; ++i) {
		std::cout << this->inputs.getValue(i) << ", ";
	}

	std::cout << this->inputs.getValue(this->inputs.ioCount) << std::endl;

	std::cout << "Output Values" << std::endl;

	std::cout << this->inputs.getValue() << std::endl << std::endl;
}
