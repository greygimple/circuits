// src/input_gate.cc
#include "../include/input_gate.h"
#include <iostream>

InputGate::InputGate(int ioCount) : Gate(ioCount, ioCount) {
	// Nothing
}

void InputGate::print(int index) {
	if (index == 0) {
		std::cout << "Board Input Values" << std::endl;
	} else if (index == 1) {
		std::cout << "Board Output Values" << std::endl;
	} else {
		std::cout << "IO Gate" << std::endl;
		std::cout << "IO Values" << std::endl;
	}

	for (int i = 0; i < this->inputs.ioCount - 1; ++i) {
		std::cout << this->inputs.getValue(i) << ", ";
	}

	std::cout << this->inputs.getValue(this->inputs.ioCount - 1) << std::endl << std::endl;
}
