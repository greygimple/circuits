
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/grey/Projects/Applications/Circuits/src/and_gate.cc" "CMakeFiles/circuits_lib.dir/src/and_gate.cc.o" "gcc" "CMakeFiles/circuits_lib.dir/src/and_gate.cc.o.d"
  "/home/grey/Projects/Applications/Circuits/src/board.cc" "CMakeFiles/circuits_lib.dir/src/board.cc.o" "gcc" "CMakeFiles/circuits_lib.dir/src/board.cc.o.d"
  "/home/grey/Projects/Applications/Circuits/src/gate.cc" "CMakeFiles/circuits_lib.dir/src/gate.cc.o" "gcc" "CMakeFiles/circuits_lib.dir/src/gate.cc.o.d"
  "/home/grey/Projects/Applications/Circuits/src/main.cc" "CMakeFiles/circuits_lib.dir/src/main.cc.o" "gcc" "CMakeFiles/circuits_lib.dir/src/main.cc.o.d"
  "/home/grey/Projects/Applications/Circuits/src/not_gate.cc" "CMakeFiles/circuits_lib.dir/src/not_gate.cc.o" "gcc" "CMakeFiles/circuits_lib.dir/src/not_gate.cc.o.d"
  "/home/grey/Projects/Applications/Circuits/src/or_gate.cc" "CMakeFiles/circuits_lib.dir/src/or_gate.cc.o" "gcc" "CMakeFiles/circuits_lib.dir/src/or_gate.cc.o.d"
  "/home/grey/Projects/Applications/Circuits/src/wire.cc" "CMakeFiles/circuits_lib.dir/src/wire.cc.o" "gcc" "CMakeFiles/circuits_lib.dir/src/wire.cc.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
