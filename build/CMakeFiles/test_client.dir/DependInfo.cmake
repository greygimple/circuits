
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/grey/Projects/Applications/Circuits/test/gate_io_test.cc" "CMakeFiles/test_client.dir/test/gate_io_test.cc.o" "gcc" "CMakeFiles/test_client.dir/test/gate_io_test.cc.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/grey/Projects/Applications/Circuits/build/external/googletest/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/grey/Projects/Applications/Circuits/build/external/googletest/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/home/grey/Projects/Applications/Circuits/build/CMakeFiles/circuits_lib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
