# CMake generated Testfile for 
# Source directory: /home/grey/Projects/Applications/Circuits
# Build directory: /home/grey/Projects/Applications/Circuits/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_client "/home/grey/Projects/Applications/Circuits/bin/test_client")
set_tests_properties(test_client PROPERTIES  _BACKTRACE_TRIPLES "/home/grey/Projects/Applications/Circuits/CMakeLists.txt;70;add_test;/home/grey/Projects/Applications/Circuits/CMakeLists.txt;0;")
subdirs("external/googletest")
