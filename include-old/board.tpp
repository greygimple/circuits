// include/board.tpp
#ifndef BOARD_TPP
#define BOARD_TPP

#include <iostream>

// Add new gate given type to list of gates
template<typename gate>
bool Board::addGate(int inCount) {
	if (gateCount != gateLen) {
		gates[gateCount] = new gate(inCount);
		++gateCount;
		return 1;
	} else {
		return 0;
	}
}

// Add new gate of given type to list of gates
template<typename gate>
bool Board::addGate(int inCount, int outCount) {
	if (gateCount != gateLen) {
		gates[gateCount] = new gate(inCount, outCount);
		++gateCount;
		return 1;
	} else {
		return 0;
	}
}


#endif // BOARD_TPP

