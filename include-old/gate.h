// include/gate.h
#ifndef GATE_H
#define GATE_H

#include "gate_io.h"

class GateIO;
class Wire;

class Gate {
public:
	// Constructor
	Gate(int inCount, int outCount);

	// Members
	GateIO inputs;
	GateIO outputs;

	// Functions
	virtual void print(int index) = 0;
	virtual bool eval() = 0;
};

#endif // GATE_H

