// include/not_gate.h
#ifndef NOT_GATE_H
#define NOT_GATE_H

#include "gate.h"
#include "gate_io.h"

class NotGate : public Gate {
public:
	// Object Init
	NotGate(int inCount, int outCount);

	// Functions
	bool eval(void) override;
	void print(int index) override;
};

#endif // NOT_GATE_H
