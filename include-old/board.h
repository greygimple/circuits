// include/board.h
#ifndef BOARD_H
#define BOARD_H

#include "wire.h"
#include "gate.h"
#include "input_gate.h"

class Board {
private:
	void sortWires(void);
public:
	// Object Init
	Board(int inCount, int outCount, int timestep);

	~Board();

	// Members
	Gate **gates;
	int gateLen;
	int gateCount;

	Wire **wires;
	int wireLen;
	int wireCount;

	//// Functions
	// Constant
	bool validIndex(int index) const;

	// Manipulate Board State
	void setInput(bool value);

	void setInput(int index, bool value); 

	template<typename gate>
	bool addGate(int inCount);

	template<typename gate>
	bool addGate(int inCount, int outCount);

	bool deleteGate(int index);

	bool addWire(int order, int g1, int g2, int oSlot, int iSlot);
	bool deleteWire(int index);

	// Scheduler
	bool update(int index, int len);
	bool runBoard(int timestep);

	void display(void);
};

#include "board.tpp"

#endif // BOARD_H
