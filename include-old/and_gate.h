// include/and_gate.h
#ifndef AND_GATE_H
#define AND_GATE_H

#include "gate.h"
#include "gate_io.h"

class AndGate : public Gate {
public:
	// Object Init
	AndGate(int inCount, int outCount);

	// Functions
	bool eval(void) override;
	void print(int index) override;
};

#endif // AND_GATE_H
