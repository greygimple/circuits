// include/or_gate.h
#ifndef OR_GATE_H
#define OR_GATE_H

#include "gate.h"
#include "gate_io.h"

class OrGate : public Gate {
public:
	// Object Init
	OrGate(int inCount, int outCount);

	// Functions
	bool eval(void) override;
	void print(int index) override;
};

#endif // OR_GATE_H
