// include/gate_io.h
#ifndef GATE_IO_H
#define GATE_IO_H

#include <tuple>
#include <iostream>

class Gate;

class GateIO {
private:
	// Constants
	static const int MAX_IO = 16;

	// Members
	bool gateIO[MAX_IO];
public:
	// Constructor
	GateIO(int ioCount);
	
	// Members
	int ioCount;

	// Funcitons
	bool validIndex(int index) const;
	bool setValue(int index, bool value);
	bool setValue(bool value);
	bool getValue(void);
	bool getValue(int index);
};

#endif // GATE_IO_H
