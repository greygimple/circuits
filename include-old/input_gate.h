// include/input_gate.h
#ifndef INPUT_GATE_H
#define INPUT_GATE_H

#include <iostream>

#include "gate.h"
#include "gate_io.h"

class InputGate : public Gate {
public:
	// Object Init
	InputGate(int ioCount);
	
	// Functions
	bool eval() override { return false; }
	void print(int index) override;
};

#endif // INPUT_GATE_H
