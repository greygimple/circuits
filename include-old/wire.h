// include/wire.h
#ifndef WIRE_H
#define WIRE_H

class Gate;

class Wire {
public:
	Wire();

	int order;
	Gate* g1;
	Gate* g2;
	int i1;
	int i2;
};

#endif // WIRE_H
