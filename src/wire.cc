// src/wire.cc

#include "../include/wire.h"
#include "../include/gate.h"

Wire::Wire(int order, Gate* src_gate, int src_channel, Gate* dest_gate, int dest_channel) {
	this->order = order;
	this->src_gate = src_gate;
	this->src_channel = src_channel;
	this->dest_gate = dest_gate;
	this->dest_channel = dest_channel;
}

bool Wire::update(void) {
	if (this->src_gate->validIndex(this->src_channel, 1) ||
		this->dest_gate->validIndex(this->dest_channel, 0)) return 0;

	this->dest_gate->setValue(
		this->src_gate->getValue(this->src_channel),
		this->dest_channel
	);

	return 1;
}
