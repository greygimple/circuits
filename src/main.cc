// src/main.cc

#include "../include/gate.h"
#include "../include/board.h"
#include "../include/and_gate.h"
#include "../include/or_gate.h"
#include "../include/not_gate.h"
#include <iostream>
#include <string>

using namespace std;

int main() {
	// Create new board
	Board my_board("My Board", 2, 1);

	my_board.addGate<AndGate>(2, 1);
	my_board.addGate<NotGate>(1);

	my_board.print();

	my_board.getGate(0)->setValue(1, 0);
	my_board.getGate(0)->setValue(1, 1);

	my_board.getGate(0)->eval();

	my_board.getGate(0)->print();

	return 0;
}
