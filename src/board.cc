// src/board.cc

#include "../include/board.h"
#include "../include/gate.h"
#include "../include/wire.h"
// #include <chrono>
// #include <thread>
// #include <algorithm>
#include <iostream>
// #include <cstdlib>
#include <string>

// Object Init
Board::Board(const std::string name, int in_count, int out_count) : Gate(in_count, out_count) {
	this->name = name;

	gates_max = 16;
	gates_count = 0;
	gates = new Gate*[gates_max];

	wires_max = 16;
	wires_count = 0;
	wires = new Wire*[wires_max];
}

Board::~Board() {
	// Clean-up array of Gate pointers
	for (int i = 0; i < gates_max; ++i) {
		delete gates[i];
	}
	delete[] gates;

	// Clean-up array of Wire Pointers
	for (int i = 0; i < wires_max; ++i) {
		delete wires[i];
	}
	delete[] wires;
	
}

Gate* Board::getGate(int index) {
	return gates[index];
}

// Functions
bool Board::deleteGate(int index) {
	if (0 <= index && index < gates_max) {
		delete gates[index];
	
		// Shift othe gates down
		for (int i = index; i < gates_count; ++i) {
			gates[i] = gates[i + 1];
		}

		gates[gates_count] = nullptr;

		--gates_count;
		return 1;
	}

	return 0;
}

void addWire(int order, int src_gate, int src_channel, int dest_gate, int dest_channel) {

}

void deleteWire(int index) {

}

void Board::eval(void) {
	std::cout << "Board Evaluation" << std::endl;
}

void Board::print(void) {
	std::cout << this->name << std::endl;
	std::cout << "Total Gates: " << gates_count << std::endl;
	std::cout << "----------------" << std::endl;
	
	std::cout << "Input Values:" << std::endl;
	for (int i = 0; i < this->in_count; ++i) {
		std::cout << this->inputs[i] << ", ";
	}
	std::cout << this->inputs[in_count] << std::endl;
	
	std::cout << "Output Values:" << std::endl;
	for (int i = 0; i < this->out_count; ++i) {
		std::cout << this->outputs[i] << ", ";
	}
	std::cout << this->outputs[out_count] << std::endl;
	std::cout << std::endl;
}
