// src/or_gate.cc

#include "../include/or_gate.h"
#include <iostream>
#include <string>

OrGate::OrGate(int in_count, int out_count) : Gate(in_count, out_count) {
	this->name = "Or Gate";
}

void OrGate::eval(void) {
	bool result = false;

	// Check for any true values in the inputs
	for (int i = 0; i < this->in_count; ++i) {
		if (this->inputs[i] == true) {
			result = true;
			break;
		}
	}

	// Set outputs channels
	for (int j = 0; j < this->out_count; ++j) {
		this->outputs[j] = result;
	}
}
