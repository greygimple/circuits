// src/not_gate.cc

#include "../include/not_gate.h"
#include <iostream>
#include <string>

NotGate::NotGate(int out_count) : Gate(1, out_count) {
	this->name = "Not Gate";
}

void NotGate::eval(void) {
	bool result = !(this->inputs[0]);

	// Set outputs channels
	for (int i = 0; i < this->out_count; ++i) {
		this->outputs[i] = result;
	}
}
