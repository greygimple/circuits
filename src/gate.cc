// src/gate.cc

#include "../include/gate.h"

#include <iostream>
#include <string>

// Constructors
Gate::Gate(int in_count, int out_count) {
	// Initialize IO Channels
	for (int i = 0; i < MAX_IN; ++i) {
		this->inputs[i] = false;
	}
	this->in_count = in_count;

	for (int i = 0; i < MAX_OUT; ++i) {
		this->outputs[i] = false;
	}
	this->out_count = out_count;
	
	// Set Gate Name
	this->name = "Base Gate";
}

// Functions
bool Gate::validIndex(int index, bool channel) const {
	if (channel == 0) {
		if (0 <= index && index < in_count) {
			return 1;
		}
	} else {
		if (0 <= index && index < out_count) {
			return 1;
		}
	}

	return 0;
}

bool Gate::setValue(bool value, int index) {
	if (validIndex(index, 0)) {
		this->inputs[index] = value;

		return 1;
	}

	return 0;
}

bool Gate::getValue(int index) {
	return this->outputs[index];
}

void Gate::print(void) {
	std::cout << this->name << std::endl;
	std::cout << "----------------" << std::endl;
	
	std::cout << "Input Values:" << std::endl;
	for (int i = 0; i < this->in_count - 1; ++i) {
		std::cout << this->inputs[i] << ", ";
	}
	std::cout << this->inputs[in_count - 1] << std::endl;
	
	std::cout << "Output Values:" << std::endl;
	for (int i = 0; i < this->out_count - 1; ++i) {
		std::cout << this->outputs[i] << ", ";
	}
	std::cout << this->outputs[out_count - 1] << std::endl;
	std::cout << std::endl;
}

void Gate::eval(void) {
	std::cout << this->name << " cannot evaluate anything" << std::endl;
}
