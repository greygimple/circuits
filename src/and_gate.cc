// src/and_gate.cc

#include "../include/and_gate.h"
#include <iostream>
#include <string>

AndGate::AndGate(int in_count, int out_count) : Gate(in_count, out_count) {
	this->name = "And Gate";
}

void AndGate::eval(void) {
	bool result = true;

	// Check for any false values in the inputs
	for (int i = 0; i < this->in_count; ++i) {
		if (this->inputs[i] == false) {
			result = false;
			break;
		}
	}

	// Set outputs channels
	for (int j = 0; j < this->out_count; ++j) {
		this->outputs[j] = result;
	}
}
