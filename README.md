# Circuits

## Download

```bash
git clone https://gitlab.com/greygimple/circuits.git

cd circuits
```

## Contact

Want to contribute and help add more features? Fork the project and open a pull request!

If you wish to contact me, you can reach out at greygimple@gmail.com

