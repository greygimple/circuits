// include/gate.h
#ifndef GATE_H
#define GATE_H

#include <string>

class Gate {
protected:
	// Constants
	static const int MAX_IN = 16;
	static const int MAX_OUT = 16;

	// Members
	bool inputs[MAX_IN];
	int in_count;

	bool outputs[MAX_OUT];
	int out_count;

	std::string name;
public:
	// Constructor
	Gate(int in_count, int out_count);

	// Functions
	bool validIndex(int index, bool channel) const;
	bool setValue(bool value, int index);
	bool getValue(int index);
	
	// Virtual
	virtual void print(void);	
	virtual void eval(void);
};

#endif // GATE_H
