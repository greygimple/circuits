// include/or_gate.h
#ifndef OR_GATE_H
#define OR_GATE_H

#include "gate.h"

class OrGate : public Gate {
public:
	// Object Init
	OrGate(int in_count, int out_count);

	// Functions
	void eval(void) override;
};

#endif // AND_GATE_H
