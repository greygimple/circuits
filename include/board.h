// include/board.h
#ifndef BOARD_H
#define BOARD_H

#include "gate.h"
#include "wire.h"
#include <string>

class Board : public Gate {
protected:
	// Members
	Gate **gates;
	int gates_max;
	int gates_count;

	Wire **wires;
	int wires_max;
	int wires_count;

	// Functions
	void sortWires(void);
public:
	// Object Init
	Board(const std::string name, int in_count, int out_count); //, int timestep); // Normal
	// Board(const Task &other); // Copy
	// Board(Board &&other); // Move
	// Board &operator=(const Board &other); // Copy assign
	// Board &operator=(Board &&other); // Move assign
	~Board(); // Destructor
	
	// Operator Overload
	// indexing operator

	// Functions
	template<typename GateType>
	bool addGate(int in_count, int out_count); // Multi input gates
	template<typename GateType>
	bool addGate(int out_count); // Single input gates
	
	Gate* getGate(int index);
	bool deleteGate(int index);

	void addWire(int order, int src_gate, int src_channel, int dest_gate, int dest_channel);
	void deleteVire(int index);
	
	// Scheduler
	// void update(int index, int len);
	// void runBoard(int timestep);

	void eval(void) override;
	void print(void) override;
};

#include "board.tpp"

#endif // BOARD_H
