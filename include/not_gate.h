// include/not_gate.h
#ifndef NOT_GATE_H
#define NOT_GATE_H

#include "gate.h"

class NotGate : public Gate {
public:
	// Object Init
	NotGate(int out_count);

	// Functions
	void eval(void) override;
};

#endif // NOT_GATE_H
