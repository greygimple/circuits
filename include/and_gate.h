// include/and_gate.h
#ifndef AND_GATE_H
#define AND_GATE_H

#include "gate.h"

class AndGate : public Gate {
public:
	// Object Init
	AndGate(int in_count, int out_count);

	// Functions
	void eval(void) override;
};

#endif // AND_GATE_H
