// include/wire.h
#ifndef WIRE_H
#define WIRE_H

#include "gate.h"

class Wire {
public:
	// Object Init
	Wire(int order, Gate* src_gate, int src_channel, Gate* dest_gate, int dest_channel);

	// Members
	int order;
	Gate* src_gate;
	int src_channel;
	Gate* dest_gate;
	int dest_channel;

	// Functions
	bool update(void);
};

#endif // WIRE_H
