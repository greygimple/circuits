// include/board.tpp
#ifndef BOARD_TPP
#define BOARD_TPP

// Add new gate of given type to list of gates
template<typename GateType>
bool Board::addGate(int in_count, int out_count) {
	if (gates_count != gates_max) {
		gates[gates_count] = new GateType(in_count, out_count);
		++gates_count;
		return 1;
	} else {
		return 0;
	}
}

// Add new gate of given type to list of gates
template<typename GateType>
bool Board::addGate(int out_count) {
	if (gates_count != gates_max) {
		gates[gates_count] = new GateType(out_count);
		++gates_count;
		return 1;
	} else {
		return 0;
	}
}

#endif // BOARD_TPP

